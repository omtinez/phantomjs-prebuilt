#!/bin/sh

INSTALL_DIR=${1:-/usr/bin}
REMOTE_URL=https://gitlab.com/omtinez/phantomjs-prebuilt/raw/master/$(uname -m)/phantomjs.tar.gz

# Test if binary exists
wget --quiet --spider --max-redirect 0 $REMOTE_URL
if [ $? != 0 ]; then
    echo "Binary for $(uname -m) not found" && exit 1
fi

# Test tmp and output location
mkdir -p $INSTALL_DIR || (echo "Could not create path for output location" && exit 1)

# Download, extract to tmp
echo "Binary for $(uname -m) found. Downloading..."
wget $REMOTE_URL -O /tmp/phantomjs.tar.gz
tar xf /tmp/phantomjs.tar.gz -C /tmp

# Remove compressed file and move to final install location
rm /tmp/phantomjs.tar.gz
mv /tmp/phantomjs $INSTALL_DIR
if [ $? != 0 ]; then
    echo "Placing binary in $INSTALL_DIR failed" && exit 1
fi

# Print and exit
echo "PhantomJS successfully installed at the following location: $INSTALL_DIR"
exit 0
