var https = require('https');
var os = require('os');
var path = require('path');
var fs = require('fs');
var tmp = require('tmp');
var mkdirp = require('mkdirp');
var targz = require('targz');
var exec = require('child_process').execSync;
 
tmp.file({'keep': true}, function _tempFileCreated(err, fpath, fd, cleanupCallback) {
    if (err) throw err;
    
    // Find out the correct binary depending on architecture
    var uname = null;
    if (os.type() === 'Windows_NT') {
		uname = 'windows';
    } else if (os.type() === 'Darwin') {
        uname = 'osx';
    } else {
        uname = exec('uname -m').toString().replace(/[\r\n]/g, '');
    }
    
    // Download the correct binary
    var stream = fs.createWriteStream(null, {fd: fd});
    var url = 'https://gitlab.com/omtinez/phantomjs-prebuilt/raw/master/' + uname + '/phantomjs.tar.gz';
    https.get(url).on('response', function (response) {
        response.pipe(stream);
        
        response.on('end', function () {
			
			// Find the top-level node_modules folder
			var currPath = path.resolve('..');
			while (path.resolve(currPath).endsWith('node_modules') ||
				fs.readdirSync(path.join(currPath, '..')).indexOf('node_modules') !== -1) {
				currPath = path.join(currPath, '..');
			}
			
			// We shouldn't have to create a node_modules folder, otherwise just go back to original path
			var bindir = path.resolve(path.join(currPath), 'node_modules', '.bin');
			if (fs.readdirSync(currPath).indexOf('node_modules') === -1) {
				bindir = path.resolve(path.join('.', 'node_modules', '.bin'));
			}
            
            // Uncompress binary and move it to the bin directory
            mkdirp.sync(bindir);
            targz.decompress({
                src: fpath,
                dest: path.join(bindir, 'phantomjs' + os.type() === 'Windows_NT' ? '.exe' : '')
            }, function(err){
                if(err) {
                    console.log('Error decompressing PhantomJS binary');
                    console.log(err);
                } else {
                    console.log('PhantomJS binary location:', path.resolve(bindir));
                }
            });
        });
    });
});
